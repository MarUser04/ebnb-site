import "./App.css";
import Routes from "./Routes/Routes";

//Context
import EbnbState from "./context/EbnbState";

function App() {
  return (
    <EbnbState>
      <Routes />
    </EbnbState>
  );
}

export default App;
