import React from "react";
import Layout from "../components/Layout";

export const RouterWrapper = ({ component: Component }) => {
  return (
    <Layout>
      <Component />
    </Layout>
  );
};
