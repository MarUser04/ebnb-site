import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useLocation,
} from "react-router-dom";

//Components
import Home from "../containers/Home";
import Results from "../containers/Results";
import Detail from "../containers/Detail";
import { RouterWrapper } from "./RouterWrapper";

const Routes = () => {
  return (
    <Router>
      <ScrollToTop />
      <Switch>
        <Route exact path="/">
          <RouterWrapper component={Home} />
        </Route>
        <Route path="/results">
          <RouterWrapper component={Results} />
        </Route>
        <Route path="/detail">
          <RouterWrapper component={Detail} />
        </Route>
      </Switch>
    </Router>
  );
};

const ScrollToTop = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
};

export default Routes;
