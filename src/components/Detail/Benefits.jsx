import React from "react";

//Assets
import StarDetailIcon from "../../assets/svg/star-details.svg";
import Sound from "../../assets/svg/sound.svg";
import Rules from "../../assets/svg/rules.svg";
import Space from "../../assets/svg/space.svg";
import Politics from "../../assets/svg/politics.svg";

const Benefits = () => {
  return (
    <div className="w-full">
      <div className="flex flex-row w-full items-start py-4">
        <img className="mt-1 mr-5" src={StarDetailIcon} alt="Star details" />
        <div className="flex flex-col">
          <h2 className="text-xl font-poppins-semibold leading-6 mb-1">
            Limpieza avanzada
          </h2>
          <span className="text-base text-gray-600 leading-5 font-inter">
            Esta sala se mantiene siempre en constante limpieza y desifencción
            ante el COVID - 19.
          </span>
        </div>
      </div>
      <div className="flex flex-row w-full items-start py-4">
        <img className="mt-1 mr-5" src={Sound} alt="Sound details" />
        <div className="flex flex-col">
          <h2 className="text-xl font-poppins-semibold leading-6 mb-1">
            Sonido privado
          </h2>
          <span className="text-base text-gray-600 leading-5 font-inter">
            Mantiene tus reuniones privadas sin problema a que se escape alguna
            información.
          </span>
        </div>
      </div>
      <div className="flex flex-row w-full items-start py-4">
        <img className="mt-1 mr-5" src={Space} alt="Space details" />
        <div className="flex flex-col">
          <h2 className="text-xl font-poppins-semibold leading-6 mb-1">
            Espacio amplio
          </h2>
          <span className="text-base text-gray-600 leading-5 font-inter">
            Contiene un espacio amplio para poder movilizarse sin problemas y
            exponer todos los puntos de tu reunión en todo momento y espacio.
          </span>
        </div>
      </div>
      <div className="flex flex-row w-full items-start py-4">
        <img className="mt-1 mr-5" src={Politics} alt="Politics details" />
        <div className="flex flex-col">
          <h2 className="text-xl font-poppins-semibold leading-6 mb-1">
            Política de cancelación
          </h2>
          <div className="flex flex-row justify-center items-center">
            <span className="text-base text-gray-600 leading-5 mr-1 font-inter">
              Si deseas cancelar tu reservación debes considerar ciertos
              detalles{" "}
              <span className="font-semibold text-black border-black border-b cursor-pointer font-inter">
                ver más detalles.
              </span>
            </span>
          </div>
        </div>
      </div>
      <div className="flex flex-row w-full items-start py-4">
        <img className="mt-1 mr-5" src={Rules} alt="Rules details" />
        <div className="flex flex-col">
          <h2 className="text-xl font-poppins-semibold leading-6 mb-1">
            Normas de la sala
          </h2>
          <span className="text-base text-gray-600 leading-5 font-inter">
            La sala indica que cuides los muebles que estan dentro de ella, así
            como apagar el aire y la luz al terminar tu sesión.
          </span>
        </div>
      </div>
    </div>
  );
};

export default Benefits;
