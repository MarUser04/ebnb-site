import React, { useState, memo } from "react";
import DateFnsUtils from "@date-io/date-fns";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";

const defaultMaterialTheme = createMuiTheme({
  palette: {
    primary: {
      main: "rgb(237,25,142)",
    },
  },
});

const Calendar = () => {
  const [date, changeDate] = useState(new Date());
  const [nextMonth, changeNextMonth] = useState(
    new Date().setMonth(date.getMonth() + 1)
  );
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <div className="flex flex-col items-start  w-full">
        <h1 className="text-2xl font-poppins-semibold leading-8 mb-1">
          Seleccionar la fecha de reserva
        </h1>
        <span className="text-base font-inter text-gray-600 leading-6">
          Agregar las/s fecha/s que deseas reservar tu sala.
        </span>
        <div className="mt-5 flex flex-col md:flex-row w-full items-center sm:justify-start ">
          <div>
            <ThemeProvider theme={defaultMaterialTheme}>
              <DatePicker
                autoOk
                variant="static"
                openTo="date"
                id="currentMonth"
                value={date}
                onChange={changeDate}
                disableToolbar={true}
                leftArrowIcon={false}
                rightArrowIcon={false}
              />
            </ThemeProvider>
          </div>
          <div className="hidden lg:block">
            <ThemeProvider theme={defaultMaterialTheme}>
              <DatePicker
                autoOk
                variant="static"
                id="nextMonth"
                value={nextMonth}
                onChange={changeNextMonth}
                disableToolbar={true}
                leftArrowIcon={false}
              />
            </ThemeProvider>
          </div>
        </div>
      </div>
    </MuiPickersUtilsProvider>
  );
};

export default memo(Calendar);
