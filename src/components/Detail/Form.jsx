import React from "react";
//Asset
import StarIcon from "../../assets/svg/star.svg";

const Form = ({ stars, hour }) => {
  return (
    <div className="flex flex-col w-full mb-10 md:mb-0 md:w-11/12 md:mx-12 border border-gray-400 rounded-xl">
      <div className="flex flex-row flex-wrap w-full justify-between">
        <div className="flex flex-row flex-wrap mx-6 mt-6 items-center">
          <h1 className="font-poppins-semibold text-base leading-7 mr-1">
            Disponible
          </h1>
          <span className="text-sm font-inter leading-5">desde {hour}</span>
        </div>
        <div className="flex flex-row items-center mx-6 mt-6">
          <img className="mr-1" src={StarIcon} alt="Star icon" />
          <span className="font-bold mr-1 font-inter">{stars}</span>
        </div>
      </div>
      <form className="mx-6 mt-5">
        <div className="floating-input relative">
          <select
            className="focus:outline-none text-sm w-full leading-5 font-inter-semibold mt-3 mb-0 p-3 pt-5 pb-2 border rounded-md rounded-b-none border-gray-300"
            type="text"
            placeholder="Actividad"
            id="activities"
            defaultValue="default"
          >
            <option disabled value="default">
              ¿Qué actividad realizaras?
            </option>
            <option value="dev">Dev</option>
            <option value="gaming">Gaming</option>
          </select>
          <label
            htmlFor="activities"
            className="absolute text-gray-400 text-xs top-0 left-0 px-3 py-4"
          >
            Actividad
          </label>
        </div>
        <div className="flex flex-row w-full">
          <div className="floating-input relative h-max w-6/12">
            <input
              className="focus:outline-none w-full text-base leading-5 font-inter  my-0 py-3 pl-3 border border-b-0 border-t-0 border-gray-300"
              type="text"
              placeholder="Llegada"
              id="arrival"
            />
            <label
              htmlFor="arrival"
              className="absolute text-gray-400 top-0 left-0 px-3 py-2  pointer-events-none transform origin-left transition-all duration-100 ease-in-out "
            >
              Llegada
            </label>
          </div>
          <div className="floating-input relative w-6/12">
            <input
              className="focus:outline-none text-base w-full leading-5 font-inter my-0 py-3 pl-3 border border-b-0 border-t-0 border-gray-300"
              type="text"
              placeholder="Salida"
              id="leaving"
            />
            <label
              htmlFor="leaving"
              className="absolute text-gray-400 top-0 left-0 px-3 py-2  pointer-events-none transform origin-left transition-all duration-100 ease-in-out "
            >
              Salida
            </label>
          </div>
        </div>
        <div className="floating-input relative">
          <input
            className="focus:outline-none text-base w-full leading-5 font-inter  my-2 mt-0 p-3 border rounded-md rounded-t-none border-gray-300"
            type="number"
            placeholder="Participantes"
            id="participantes"
          />
          <label
            htmlFor="participantes"
            className="absolute text-gray-400 top-0 left-0 px-3 py-3 h-full pointer-events-none transform origin-left transition-all duration-100 ease-in-out "
          >
            Participantes
          </label>
        </div>
      </form>
      <button className="flex mx-6 my-4 font-inter-semibold text-base leading-5	 bg-pink-600 p-2 tracking-wider text-white font-semibold rounded-xl justify-center focus:outline-none">
        Buscar sala disponible
      </button>
    </div>
  );
};

export default Form;
