import React from "react";

//Assets
import Details1 from "../../assets/images/details-1.png";
import Details2 from "../../assets/images/details-2.png";
import Details3 from "../../assets/images/details-3.png";
import Details4 from "../../assets/images/details-4.png";
import Details5 from "../../assets/images/details-5.png";

const Gallery = () => {
  return (
    <div className="grid grid-rows-2 md:grid-cols-8 gap-2">
      <img
        className="row-span-2 col-span-4 h-56 md:h-128 w-full object-cover rounded-t-2xl md:rounded-tr-none md:rounded-l-2xl"
        src={Details1}
        alt="Detail room"
      />
      <img
        className="row-span-1 col-span-2 h-40 md:h-52 w-full object-cover rounded-bl-2xl md:rounded-bl-none"
        src={Details2}
        alt="Detail room"
      />
      <img
        className="row-span-1 col-span-2 h-40 md:h-52  w-full object-cover rounded-br-2xl md:rounded-br-none md:rounded-tr-2xl"
        src={Details3}
        alt="Detail room"
      />
      <img
        className="hidden md:block row-span-1 col-span-2 h-52 w-full object-cover"
        src={Details4}
        alt="Detail room"
      />
      <img
        className="hidden md:block row-span-1 col-span-2 h-52 w-full object-cover md:rounded-br-2xl"
        src={Details5}
        alt="Detail room"
      />
    </div>
  );
};

export default Gallery;
