import React from "react";

//Asset
import Board from "../../assets/svg/board.svg";
import Air from "../../assets/svg/air.svg";
import Wifi from "../../assets/svg/wifi.svg";
import TV from "../../assets/svg/tv.svg";

const Services = () => {
  return (
    <div className="flex flex-col items-start">
      <h1 className="text-2xl font-poppins-semibold leading-8">Servicios</h1>
      <div className="flex flex-row mt-7 mb-10 lg:justify-between w-full lg:w-2/4">
        <div className="flex flex-col mr-12 sm:mr-28 lg:mr-0">
          <div className="mb-5 flex flex-row">
            <img className="mr-2" src={Board} alt="Board icon" />
            <h2 className="text-sm leading-5 font-inter">Pizarra de Plumón</h2>
          </div>
          <div className="flex flex-row">
            <img className="mr-2" src={Air} alt="Air icon" />
            <h2 className="text-sm leading-5 font-inter">
              {" "}
              Aire Acondicionado
            </h2>
          </div>
        </div>
        <div className="flex flex-col">
          <div className="mb-5 flex flex-row">
            <img className="mr-2" src={Wifi} alt="Wifi icon" />
            <h2 className="text-sm leading-5 font-inter">Wifi</h2>
          </div>
          <div className="flex flex-row">
            <img className="mr-2" src={TV} alt="TV icon" />
            <h2 className="text-sm leading-5 font-inter"> Smart TV</h2>
          </div>
        </div>
      </div>
      <button className="border border-gray-900 py-2 px-4 rounded-xl leading-5 font-inter-semibold text-base">
        Mostrar los 24 servicios
      </button>
    </div>
  );
};

export default Services;
