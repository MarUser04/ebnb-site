import React from "react";

const Footer = () => {
  return (
    <div className="m-2 flex flex-row justify-center">
      <span className="text-center text-base leading-4 font-roboto">
        © ebnb.inc, 2021 • Todos los Derechos Reservados • Proyecto basado en
        &nbsp; <span className="text-pink-600">airbnb.com</span>
      </span>
    </div>
  );
};

export default Footer;
