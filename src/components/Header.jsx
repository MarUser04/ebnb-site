import React, { useState, useContext } from "react";
import { Link, useHistory } from "react-router-dom";

//Context
import EbnbContext from "../context/EbnbContext";

//Components
import ModalLogin from "./ModalLogin";

//Assets
import Logo from "../assets/svg/logo.svg";
import Search from "../assets/svg/search-icon.svg";
import UserIcon from "../assets/svg/user.svg";
import MenuIcon from "../assets/svg/menu.svg";
import Menu from "./Menu";

const Header = () => {
  const { searchRooms, setFilters, getUser, user, logout } = useContext(
    EbnbContext
  );
  const history = useHistory();
  const [menuActive, setMenuActive] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [searchValue, setSearchValue] = useState("");

  const searchRoom = () => {
    searchRooms(searchValue);
    setFilters({ mainFilter: "", subFilter: "" });
    history.push("/results");
  };

  return (
    <div className="flex flex-row w-full h-16 items-center justify-between md:justify-around shadow-md mb-2 lg:mb-0">
      <div className="ml-6 md:ml-0">
        <Link to="/">
          <img src={Logo} alt="Ebnb Logo" />
        </Link>
      </div>
      <div className="hidden md:block">
        <div className="relative md:w-96 flex flex-wrap items-center">
          <input
            className="relative border rounded-3xl w-full font-inter leading-5 py-1.5 px-3 text-black focus:outline-none placeholder-black"
            type="text"
            placeholder="Busca una sala"
            onChange={(e) => setSearchValue(e.target.value)}
            onKeyPress={(e) => e.key === "Enter" && searchRoom()}
          />
          <div
            onClick={() => searchRoom()}
            className="absolute bg-pink-600 z-10 leading-snug font-normal  bg-transparent rounded-3xl text-base items-center justify-center right-0 p-1.5 m-1.5 cursor-pointer"
          >
            <img src={Search} alt="Search form icon" />
          </div>
        </div>
      </div>
      <div className="flex flex-row items-center mr-6 md:mr-0">
        <span className="font-bold lg:mr-4 mr-2 font-inter leading-5 hidden md:block">
          Se un host
        </span>
        <div className="flex flex-row border relative border-gray-200  rounded-3xl pr-2 pl-2 py-1">
          <button
            id="menuButton"
            className="focus:outline-none"
            onClick={() => setMenuActive((prevState) => !prevState)}
          >
            <img
              id="menuButtonImg"
              className="mr-2"
              src={MenuIcon}
              alt="Menu icon"
            />
          </button>
          <img src={UserIcon} alt="User icon" />
          {menuActive && (
            <Menu
              setShowModal={setShowModal}
              setMenuActive={setMenuActive}
              user={user}
              logout={logout}
            />
          )}
        </div>
      </div>
      {showModal ? (
        <ModalLogin setShowModal={setShowModal} getUser={getUser} />
      ) : null}
    </div>
  );
};

export default Header;
