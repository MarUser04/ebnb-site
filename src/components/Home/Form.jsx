import React, { useState } from "react";
import classNames from "classnames";
import { useHistory } from "react-router-dom";

const Form = ({ getRoomsByFormFilters, setSearchValue }) => {
  const [tab, setTab] = useState(1);
  const [activity, setActivity] = useState("");
  const [hour, setHour] = useState("");
  const [arrival, setArrival] = useState("");
  const [exit, setExit] = useState("");
  const [participants, setParticipants] = useState(0);
  const history = useHistory();

  const searchRooms = () => {
    if (tab !== 1 && activity !== "" && hour !== "") {
      getRoomsByFormFilters("hour", {
        activity,
        hour,
      });
      setSearchValue(`${activity} - ${hour}`);
      history.push("/results");
    } else if (
      activity !== "" ||
      arrival !== "" ||
      exit !== "" ||
      participants !== 0
    ) {
      const body = {
        activity,
        arrival,
        exit,
        participants,
      };
      getRoomsByFormFilters("time", body);
      const textActivity = activity ? `${activity}. ` : "";
      const textArrival = arrival ? `Llegada: ${arrival}. ` : "";
      const textExit = exit ? `Salida: ${exit}. ` : "";
      const textParticipantes =
        participants > 0 ? `${participants} Participantes.` : "";
      setSearchValue(
        `${textActivity}${textArrival}${textExit}${textParticipantes}`
      );
      history.push("/results");
    }
  };

  return (
    <div className="flex flex-col xl:w-96 lg:w-full h-max bg-white p-7 rounded-2xl">
      <h1 className="text-4xl mb-2  tracking-tight leading-9 font-poppins-semibold">
        Comienza una <br className="hidden xl:block" /> nueva aventura
      </h1>
      <span className="text-base tracking-wide font-inter leading-5">
        Busca una sala donde puedas sentirte comodo trabajando y compartiendo.
      </span>
      <div className="bg-white">
        <nav className="flex flex-col sm:flex-row">
          <button
            className={classNames(
              "text-black text-sm leading-5 py-4 px-6 block focus:outline-none  border-b-2 font-medium",
              {
                "border-black": tab === 1,
                "border-none": tab !== 1,
              }
            )}
            onClick={() => setTab(1)}
          >
            Salas
          </button>
          <button
            className={classNames(
              "text-black text-sm leading-5 py-4 px-6 block  focus:outline-none  border-b-2 font-medium",
              {
                "border-black": tab === 2,
                "border-none": tab !== 2,
              }
            )}
            onClick={() => setTab(2)}
          >
            Amenidades
          </button>
          <button
            className={classNames(
              "text-black text-sm leading-5 py-4 px-6 block  focus:outline-none  border-b-2 font-medium",
              {
                "border-black": tab === 3,
                "border-none": tab !== 3,
              }
            )}
            onClick={() => setTab(3)}
          >
            Experiencias
          </button>
        </nav>
      </div>
      <div className="flex flex-col mt-2">
        {tab === 1 && (
          <form>
            <div className="floating-input relative">
              <select
                className="focus:outline-none text-sm w-full leading-5 font-inter-semibold mt-3 mb-0 p-3 pt-5 pb-2 border rounded-md rounded-b-none border-gray-300"
                type="text"
                placeholder="Actividad"
                id="activities"
                defaultValue="default"
                onChange={(e) => setActivity(e.target.value)}
              >
                <option disabled value="default">
                  ¿Qué actividad realizaras?
                </option>
                <option value="Dev">Dev</option>
                <option value="Gaming">Gaming</option>
              </select>
              <label
                htmlFor="activities"
                className="absolute text-gray-400 text-xs top-0 left-0 px-3 py-4"
              >
                Actividad
              </label>
            </div>
            <div className="flex flex-row w-full">
              <div className="floating-input relative h-max w-6/12">
                <input
                  className="focus:outline-none w-full text-base leading-5 font-inter  my-0 py-3 pl-3 border border-b-0 border-t-0 border-gray-300"
                  type="text"
                  placeholder="Llegada"
                  id="arrival"
                  onChange={(e) => setArrival(e.target.value)}
                />
                <label
                  htmlFor="arrival"
                  className="absolute text-gray-400 top-0 left-0 px-3 py-2  pointer-events-none transform origin-left transition-all duration-100 ease-in-out "
                >
                  Llegada
                </label>
              </div>
              <div className="floating-input relative w-6/12">
                <input
                  className="focus:outline-none w-full  text-base leading-5 font-inter my-0 py-3 pl-3 border border-b-0 border-t-0 border-gray-300"
                  type="text"
                  placeholder="Salida"
                  id="leaving"
                  onChange={(e) => setExit(e.target.value)}
                />
                <label
                  htmlFor="leaving"
                  className="absolute text-gray-400 top-0 left-0 px-3 py-2  pointer-events-none transform origin-left transition-all duration-100 ease-in-out "
                >
                  Salida
                </label>
              </div>
            </div>
            <div className="floating-input relative">
              <input
                className="focus:outline-none text-base w-full leading-5 font-inter  my-2 mt-0 p-3 border rounded-md rounded-t-none border-gray-300"
                type="number"
                placeholder="Participantes"
                id="participantes"
                onChange={(e) => setParticipants(e.target.value)}
              />
              <label
                htmlFor="participantes"
                className="absolute text-gray-400 top-0 left-0 px-3 py-3 h-full pointer-events-none transform origin-left transition-all duration-100 ease-in-out "
              >
                Participantes
              </label>
            </div>
          </form>
        )}
        {(tab === 2 || tab === 3) && (
          <form>
            <div className="floating-input relative">
              <select
                className="focus:outline-none text-sm w-full leading-5 font-inter-semibold mt-3 mb-0 p-3 pt-5 pb-2 border rounded-md rounded-b-none border-gray-300"
                type="text"
                placeholder="Actividad"
                id="activities"
                defaultValue="default"
                onChange={(e) => setActivity(e.target.value)}
              >
                <option disabled value="default">
                  ¿Qué deseas hacer?
                </option>
                <option value="Dev">Dev</option>
                <option value="Gaming">Gaming</option>
              </select>
              <label
                htmlFor="activities"
                className="absolute text-gray-400 text-xs top-0 left-0 px-3 py-4"
              >
                Actividad
              </label>
            </div>
            <div className="floating-input relative">
              <input
                className="focus:outline-none text-base w-full leading-5 font-inter  my-2 mt-0 p-3 border rounded-md rounded-t-none border-gray-300"
                type="text"
                placeholder="Hora de ida"
                id="time"
                onChange={(e) => setHour(e.target.value)}
              />
              <label
                htmlFor="time"
                className="absolute text-gray-400 top-0 left-0 px-3 py-3 h-full pointer-events-none transform origin-left transition-all duration-100 ease-in-out "
              >
                Hora del día (Ej. 2:00pm)
              </label>
            </div>
          </form>
        )}
      </div>
      <span className="text-xs w-auto leading-4 text-gray-600">
        Estos datos son importantes para darte un mejor resuldato de tu
        búsqueda.
      </span>
      <button
        onClick={() => searchRooms()}
        className="flex mt-3 leading-5 text-base bg-pink-600 w-52 p-2 tracking-wider text-white font-semibold rounded-xl justify-center focus:outline-none"
      >
        Buscar sala disponible
      </button>
    </div>
  );
};

export default Form;
