import React from "react";
import { Link } from "react-router-dom";

const Room = ({ room, img }) => {
  return (
    <div className="flex flex-row h-24 w-max">
      <div>
        <img
          className="object-fill rounded-2xl mr-4"
          src={img}
          alt={room.title}
        />
      </div>
      <div className="flex flex-col">
        <Link to="/results">
          <h2 className="text-xl font-bold leading-6 font-inter">
            {room.title}
          </h2>
        </Link>
        <span className="text-base leading-5 font-inter text-gray-400">
          {room.ubication}
        </span>
      </div>
    </div>
  );
};
export default Room;
