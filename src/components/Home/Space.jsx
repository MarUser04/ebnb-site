import React from "react";

const Space = ({ space, img }) => {
  return (
    <div className="flex flex-col w-full sm:w-9/12 mt-6 mb-4 md:mb-0 md:mt-0 md:m-4">
      <img className="w-full" src={img} alt="Espacio de trabajo" />
      <h3 className="font-bold text-xl mt-6 mb-1 leading-6 font-poppins">
        {space.title}
      </h3>
      <span className="text-base font-inter leading-5">{space.text}</span>
    </div>
  );
};

export default Space;
