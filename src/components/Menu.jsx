import React, { useRef } from "react";
import { Link } from "react-router-dom";
import { useClickAway } from "react-use";
import classNames from "classnames";

const Menu = ({ setShowModal, setMenuActive, user, logout }) => {
  const ref = useRef(null);

  const openModal = () => {
    setMenuActive(false);
    setShowModal(true);
  };

  useClickAway(ref, (e) => {
    if (e.target.id !== "menuButton" && e.target.id !== "menuButtonImg") {
      setMenuActive(false);
    }
  });

  return (
    <div
      ref={ref}
      className="bg-white w-52 top-12 rounded-2xl flex flex-col right-0 z-10 text-black absolute border border-gray-400"
    >
      {Object.entries(user).length > 0 ? (
        <div className="flex flex-col border-gray-300 border-b p-4 font-inter-semibold text-sm leading-5 cursor-pointer">
          <span>{`${user.firstname} ${user.lastname}`}</span>
          <span>País: {user.country}</span>
          <span>Posición: {user.position}</span>
        </div>
      ) : (
        <div
          onClick={() => openModal()}
          className="border-gray-300 border-b p-4 font-inter-semibold text-sm leading-5 cursor-pointer"
        >
          <span>Iniciar sesión</span>
        </div>
      )}
      <div className="border-gray-300 border-b p-4 font-inter-semibold text-sm leading-5 cursor-pointer">
        <Link to="/results" onClick={() => setMenuActive(false)}>
          <span>Ver salas cercanas</span>
        </Link>
      </div>
      <div className="border-gray-300 border-b p-4 font-inter-semibold text-sm leading-5 cursor-pointer">
        <span>Organiza una experiencia</span>
      </div>
      <div
        className={classNames(
          "p-4 font-inter-semibold text-sm leading-5 cursor-pointer",
          {
            "border-gray-300 border-b": Object.entries(user).length > 0,
          }
        )}
      >
        <span>Ayuda</span>
      </div>
      {Object.entries(user).length > 0 && (
        <div
          className="p-4 font-inter-semibold text-sm leading-5 cursor-pointer"
          onClick={() => logout()}
        >
          <span>Salir</span>
        </div>
      )}
    </div>
  );
};

export default Menu;
