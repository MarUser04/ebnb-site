import React, { useState } from "react";

//Assets
import Close from "../assets/svg/close.svg";
import Email from "../assets/svg/email.svg";
import Google from "../assets/svg/google.svg";
import Facebook from "../assets/svg/facebook.svg";
import Apple from "../assets/svg/apple.svg";

const ModalLogin = ({ setShowModal, getUser }) => {
  const [code, setCode] = useState("");
  const [country, setCountry] = useState("");

  const login = () => {
    getUser({
      country,
      code,
    });
    setShowModal(false);
  };

  return (
    <>
      <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-9/12  md:w-max bg-white rounded-xl">
          <div className="flex flex-col">
            <div className="flex flex-row my-3 pb-3 justify-between border-gray-300 border-b w-full">
              <img
                src={Close}
                alt="Close icon"
                className="ml-7 cursor-pointer"
                onClick={() => setShowModal(false)}
              />
              <h2 className="font-inter-semibold text-base leading-5 pr-10">
                Inicia sesión
              </h2>
              <div></div>
            </div>
            <div className="flex flex-col m-6 mt-2 mb-4">
              <input
                className="focus:outline-none text-base leading-5 font-inter my-2 mb-0 p-3 border rounded-lg rounded-b-none border-gray-300"
                type="text"
                placeholder="País/Región"
                onChange={(e) => setCountry(e.target.value)}
              />

              <input
                className="focus:outline-none text-base leading-5 font-inter  my-2 mt-0 p-3 border rounded-lg rounded-t-none border-gray-300"
                type="text"
                placeholder="Código de empleado"
                onChange={(e) => setCode(e.target.value)}
              />
              <span className="text-black font-inter-semibold text-xs leading-4">
                Te estaremos enviando un correo de verificación para continuar.
              </span>
              <button
                onClick={() => login()}
                className="flex mt-3 leading-5 text-base bg-pink-600 p-2 tracking-wider text-white font-semibold rounded-xl justify-center"
              >
                Continuar
              </button>
            </div>
            <div className="flex flex-row items-center">
              <div className="border-gray-300 border-b w-full ml-5 mr-2"></div>
              <div className="font-bold">o</div>
              <div className="border-gray-300 border-b w-full ml-2 mr-5"></div>
            </div>
            <div className="flex flex-col items-center justify-center md:justify-between md:flex-row m-2 md:m-6 md:mx-4 md:w-4/5 ">
              <button className="btn-filter-option flex items-center py-2  md:mr-2 btn-filter-options-selected mb-3 md:mb-0">
                <img className="mr-2 md:mr-5" src={Email} alt="Email icon" />
                <span className="font-inter text-xs md:text-sm leading-5">
                  {" "}
                  Continuar con correo electrónico
                </span>
              </button>
              <div className="flex md:w-3/12">
                <img
                  className="mr-7 h-10 w-10"
                  src={Google}
                  alt="Google icon"
                />
                <img
                  className="mr-7 h-10 w-10"
                  src={Facebook}
                  alt="Facebook icon"
                />
                <img className="mr-1 h-10 w-10" src={Apple} alt="Apple icon" />
              </div>
            </div>
            <span className="font-inter text-xs leading-4 m-6 mt-0">
              ¿No conoce tu código de empleado?{" "}
              <span className="border-black border-b cursor-pointer text-black font-inter-semibold">
                Solicitalo aquí
              </span>
            </span>
          </div>
        </div>
      </div>
      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
    </>
  );
};

export default ModalLogin;
