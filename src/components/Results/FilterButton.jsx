import React from "react";
import classNames from "classnames";

export const FilterButton = ({ text, value, filterValue, setFilterValue }) => {
  return (
    <button
      className={classNames(
        "btn-filter-option py-1 mr-2 font-poppins text-sm leading-5",
        {
          "btn-filter-options-selected": filterValue === value,
        }
      )}
      onClick={() =>
        value === "Aire acondicionado"
          ? setFilterValue(
              "Aire acondicionado",
              "description",
              "Aire acondicionado"
            )
          : setFilterValue((prevState) => (prevState === value ? "" : value))
      }
    >
      {text}
    </button>
  );
};
