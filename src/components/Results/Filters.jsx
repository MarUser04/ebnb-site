import React, { useState, useEffect } from "react";
import SubFilters from "./SubFilters";
import { FilterButton } from "./FilterButton";

/*
  filterRoom: context method for filters rooms with category and subcategory,
  getRooms: get all rooms method from databases,
  filters: filters object value from context. It has main and sub filters,
  setFilters: update filters object in context 
*/

const Filters = ({ filterRoom, getRooms, filters, setFilters }) => {
  const [mainFilter, setMainFilter] = useState(filters.mainFilter);
  const [subFilter, setSubFilter] = useState(filters.subFilter);
  const mainFilters = [
    "Flexibilidad de cancelación",
    "Tipo de actividad",
    "Área",
    "Aire acondicionado",
  ];
  const placesFilters = ["1ra Planta", "2da Planta"];
  const flexibilityFilters = ["Permitida", "No Permitida"];
  const activityFilters = ["Dev", "Gaming"];

  useEffect(() => {
    setMainFilter(filters.mainFilter);
    setSubFilter(filters.subFilter);
  }, [filters]);

  const updateRoomsBySubFilters = (currentValue, category) => {
    setSubFilter((prevState) =>
      prevState === currentValue ? "" : currentValue
    );
    if (currentValue !== subFilter) {
      filterRoom(category, currentValue);
      setFilters({ mainFilter: mainFilter, subFilter: currentValue });
    } else {
      getRooms();
      setFilters({ mainFilter: mainFilter, subFilter: "" });
    }
  };

  const updateRoomsByMainFilter = (
    currentValue,
    category,
    filterValue = ""
  ) => {
    setMainFilter((prevState) =>
      prevState === currentValue ? "" : currentValue
    );
    if (currentValue !== mainFilter) {
      filterRoom(category, filterValue);
      setFilters({ mainFilter: currentValue, subFilter: "" });
    } else {
      getRooms();
      setFilters({ mainFilter: "", subFilter: "" });
    }
  };

  return (
    <div className="flex flex-col  mt-6">
      <div className="flex flex-col md:flex-row mb-2 space-y-2 md:space-y-0">
        {mainFilters.map((filter, index) => (
          <FilterButton
            key={index}
            text={filter}
            value={filter}
            filterValue={mainFilter}
            setFilterValue={
              filter === "Aire acondicionado"
                ? updateRoomsByMainFilter
                : setMainFilter
            }
          />
        ))}
      </div>
      {mainFilter !== "" && mainFilter !== "Aire acondicionado" && (
        <div className="block md:hidden">
          <h2 className="font-poppins-semibold m-3">SubFiltros</h2>
        </div>
      )}
      {mainFilter === "Flexibilidad de cancelación" && (
        <SubFilters
          filters={flexibilityFilters}
          subFilter={subFilter}
          mainFilter="flexibility"
          updateRoomsBySubFilters={updateRoomsBySubFilters}
        />
      )}
      {mainFilter === "Tipo de actividad" && (
        <SubFilters
          filters={activityFilters}
          subFilter={subFilter}
          mainFilter="activity"
          updateRoomsBySubFilters={updateRoomsBySubFilters}
        />
      )}
      {mainFilter === "Área" && (
        <SubFilters
          filters={placesFilters}
          subFilter={subFilter}
          mainFilter="place"
          updateRoomsBySubFilters={updateRoomsBySubFilters}
        />
      )}
    </div>
  );
};

export default Filters;
