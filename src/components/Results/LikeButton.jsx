import React, { memo } from "react";

const LikeButton = ({ likeIcon, likeEmptyIcon, like }) => {
  return (
    <img
      className="cursor-pointer"
      src={like ? likeIcon : likeEmptyIcon}
      alt="Like Icon"
    />
  );
};

export default memo(LikeButton);
