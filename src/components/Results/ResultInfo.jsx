import React, { memo } from "react";
import { Link } from "react-router-dom";
import classNames from "classnames";

//Components
import LikeButton from "./LikeButton";

//Assets
import StarIcon from "../../assets/svg/star.svg";
import LikeIcon from "../../assets/svg/Like.svg";
import LikeEmptyIcon from "../../assets/svg/LikeEmpty.svg";
import LikeWhiteIcon from "../../assets/svg/LikeWhite.svg";
import LikeWhiteEmptyIcon from "../../assets/svg/LikeWhiteEmpty.svg";

const ResultInfo = ({ result, img, lastItem, updateLikeRoom }) => {
  return (
    <div
      className={classNames("flex flex-col md:flex-row w-full pb-4 mb-6", {
        "border-gray-300 border-b": !lastItem,
      })}
    >
      <div className="relative sm:w-full md:w-100">
        <img
          className="w-full h-64 md:h-52 rounded-xl"
          src={img}
          alt="Resultado de búsqueda"
        />
        <div className="absolute top-1 flex flex-row w-full justify-between pr-2.5">
          <button className="bg-white text-gray-900 px-2 py-2 font-bold rounded-lg m-4 text-base leading-4 font-inter">
            DISPONIBLE AHORA
          </button>
          <div
            className="sm:invisible visible p-2"
            onClick={() => updateLikeRoom(result._id, !result.like)}
          >
            <LikeButton
              likeIcon={LikeWhiteIcon}
              likeEmptyIcon={LikeWhiteEmptyIcon}
              like={result.like}
            />
          </div>
        </div>
      </div>
      <div className="flex flex-col w-full md:ml-8 mt-6 md:mt-0">
        <div className="flex flex-row w-full justify-between">
          <div className="flex flex-col">
            <span className="text-sm font-inter leading-4 mb-1">
              {result.place}
            </span>
            <Link
              to={{
                pathname: "/detail",
                state: { room: result },
              }}
            >
              <h1 className="font-bold text-2xl font-inter leading-6">
                {result.title}
              </h1>
            </Link>
          </div>
          <div
            className="sm:visible invisible"
            onClick={() => updateLikeRoom(result._id, !result.like)}
          >
            <LikeButton
              likeIcon={LikeIcon}
              likeEmptyIcon={LikeEmptyIcon}
              like={result.like}
            />
          </div>
        </div>
        <div className="mt-4 w-12 border-gray-400 border-b"></div>
        <span className="mt-2 text-gray-400 font-inter text-base leading-5">
          {result.description}
        </span>
        <span className="mt-1 text-gray-400 text-sm font-inter-semibold leading-5">
          Flexibilidad de cancelación: {result.flexibility}
        </span>
        <span className="mt-1 text-gray-400 text-sm font-inter-semibold leading-5">
          Tipo de actividad: {result.activity}
        </span>
        <span className="mt-1 text-gray-400 text-sm font-inter-semibold leading-5">
          Disponible entre: {result.arrival} - {result.exit}
        </span>
        <div className="flex flex-row justify-between mt-4 ">
          <div className="flex flex-row items-center justify-center">
            <img className="mr-1 pb-1" src={StarIcon} alt="Star icon" />
            <span className="font-semibold mr-1 font-inter text-base leading-5">
              {result.stars}
            </span>
            <span className="text-gray-400 font-inter text-base leading-5">
              ({result.reviews})
            </span>
          </div>
          <span className="font-bold text-base leading-5">Disponible</span>
        </div>
      </div>
    </div>
  );
};

export default memo(ResultInfo);
