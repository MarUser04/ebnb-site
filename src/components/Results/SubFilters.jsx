import React from "react";
import classNames from "classnames";

const SubFilters = ({
  filters,
  subFilter,
  mainFilter,
  updateRoomsBySubFilters,
}) => {
  return (
    <div className="flex flex-col md:flex-row space-y-2 md:space-y-0">
      {filters.map((place, index) => (
        <button
          key={index}
          className={classNames(
            "btn-filter-option  mr-2 font-poppins text-sm leading-5",
            {
              "btn-filter-options-selected": subFilter === place,
            }
          )}
          onClick={() => updateRoomsBySubFilters(place, mainFilter)}
        >
          {place}
        </button>
      ))}
    </div>
  );
};

export default SubFilters;
