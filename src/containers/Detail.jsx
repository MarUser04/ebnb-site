import React from "react";
import { Redirect, useLocation } from "react-router";

//Pages
import DetailPage from "../pages/DetailPage";

const Detail = () => {
  const data = useLocation();
  return data.state && data.state.room ? (
    <DetailPage room={data.state.room} />
  ) : (
    <Redirect to="/" />
  );
};

export default Detail;
