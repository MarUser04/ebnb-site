import React, { useContext } from "react";

//Pages
import HomePage from "../pages/HomePage";

//Context
import EbnbContext from "../context/EbnbContext";

const Home = () => {
  const { zones, spaces, getRoomsByFormFilters, setSearchValue } = useContext(
    EbnbContext
  );
  return (
    <HomePage
      rooms={zones}
      spaces={spaces}
      getRoomsByFormFilters={getRoomsByFormFilters}
      setSearchValue={setSearchValue}
    />
  );
};

export default Home;
