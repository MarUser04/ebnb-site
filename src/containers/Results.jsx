import React, { useContext } from "react";

//Pages
import ResultsPage from "../pages/ResultsPage";

//Context
import EbnbContext from "../context/EbnbContext";

const Results = () => {
  const {
    rooms,
    searchValue,
    updateLikeRoom,
    filterRoom,
    getRooms,
    filters,
    setFilters,
    setSearchValue,
  } = useContext(EbnbContext);

  const resetAllRooms = () => {
    setFilters({ mainFilter: "", subFilter: "" });
    setSearchValue("");
    getRooms();
  };

  return (
    <ResultsPage
      results={rooms}
      searchValue={searchValue}
      updateLikeRoom={updateLikeRoom}
      filterRoom={filterRoom}
      getRooms={getRooms}
      filters={filters}
      setFilters={setFilters}
      resetAllRooms={resetAllRooms}
    />
  );
};

export default Results;
