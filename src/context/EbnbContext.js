import { createContext } from "react";

const EbnbContext = createContext();

export default EbnbContext;
