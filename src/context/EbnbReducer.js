import {
  GET_ROOMS,
  GET_ZONES,
  GET_SPACES,
  SET_SEARCH,
  UPDATE_ROOM,
  SET_FILTERS,
  GET_USER,
  LOGOUT,
} from "./types";

const EbnbReducer = (state, action) => {
  const { payload, type } = action;

  switch (type) {
    case GET_ZONES:
      return {
        ...state,
        zones: payload,
      };
    case GET_ROOMS:
      return {
        ...state,
        rooms: payload,
      };
    case GET_SPACES:
      return {
        ...state,
        spaces: payload,
      };
    case SET_SEARCH:
      return {
        ...state,
        searchValue: payload,
      };
    case SET_FILTERS:
      return {
        ...state,
        filters: payload,
      };
    case UPDATE_ROOM:
      return {
        ...state,
        rooms: state.rooms.map((room) =>
          room._id === payload._id ? { ...room, like: payload.like } : room
        ),
      };
    case GET_USER:
      return {
        ...state,
        user: payload,
      };
    case LOGOUT:
      return {
        ...state,
        user: payload,
      };
    default:
      return state;
  }
};

export default EbnbReducer;
