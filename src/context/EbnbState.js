import React, { useReducer, useEffect, useCallback } from "react";
import axios from "../config/axios";
import EbnbReducer from "./EbnbReducer";
import EbnbContext from "./EbnbContext";
import {
  GET_ROOMS,
  GET_ZONES,
  GET_SPACES,
  SET_SEARCH,
  UPDATE_ROOM,
  SET_FILTERS,
  GET_USER,
  LOGOUT,
} from "./types";

const EbnbState = (props) => {
  const initialState = {
    zones: [],
    spaces: [],
    rooms: [],
    searchValue: "",
    filters: {
      mainFilter: "",
      subFilter: "",
    },
    user: {},
  };

  const [state, dispatch] = useReducer(EbnbReducer, initialState);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await axios.get("/zones");
        dispatch({ type: GET_ZONES, payload: res.data });
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await axios.get("/workspaces");
        dispatch({ type: GET_SPACES, payload: res.data });
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await axios.get("/rooms");
        dispatch({ type: GET_ROOMS, payload: res.data });
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);

  const getRooms = async () => {
    try {
      const res = await axios.get("/rooms");
      dispatch({ type: GET_ROOMS, payload: res.data });
    } catch (error) {
      console.error(error);
    }
  };

  const setSearchValue = (value) => {
    dispatch({ type: SET_SEARCH, payload: value });
  };

  const searchRooms = useCallback(async (value) => {
    try {
      const res = await axios.get(`/rooms/${value}`);
      dispatch({ type: GET_ROOMS, payload: res.data });
      setSearchValue(value);
    } catch (error) {
      console.error(error);
    }
  }, []);

  const updateLikeRoom = useCallback(async (id, value) => {
    try {
      const config = { headers: { "Content-Type": "application/json" } };
      const res = await axios.put(`/rooms/${id}`, { like: value }, config);
      dispatch({ type: UPDATE_ROOM, payload: res.data });
    } catch (error) {
      console.error(error);
    }
  }, []);

  const filterRoom = async (filter, value) => {
    try {
      const res = await axios.get(`/rooms/${filter}/${value}`);
      dispatch({ type: GET_ROOMS, payload: res.data });
    } catch (error) {
      console.error(error);
    }
  };

  const setFilters = (value) => {
    dispatch({ type: SET_FILTERS, payload: value });
  };

  const getUser = async (body) => {
    try {
      const config = { headers: { "Content-Type": "application/json" } };
      const res = await axios.post("/auth", body, config);
      const payload = res.data[0] ? res.data[0] : {};
      dispatch({
        type: GET_USER,
        payload,
      });
    } catch (error) {
      console.error(error);
    }
  };

  const logout = () => {
    dispatch({ type: LOGOUT, payload: {} });
  };

  const getRoomsByFormFilters = async (type, body) => {
    try {
      const config = { headers: { "Content-Type": "application/json" } };
      const res = await axios.post(`/rooms/${type}`, body, config);
      dispatch({ type: GET_ROOMS, payload: res.data });
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <EbnbContext.Provider
      value={{
        zones: state.zones,
        spaces: state.spaces,
        rooms: state.rooms,
        searchValue: state.searchValue,
        filters: state.filters,
        user: state.user,
        getRooms,
        searchRooms,
        setSearchValue,
        updateLikeRoom,
        filterRoom,
        getRoomsByFormFilters,
        setFilters,
        getUser,
        logout,
      }}
    >
      {props.children}
    </EbnbContext.Provider>
  );
};

export default EbnbState;
