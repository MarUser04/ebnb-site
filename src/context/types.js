export const GET_ZONES = "GET_ROOMS_HOMGET_ZONESE";
export const GET_ROOMS = "GET_ROOMS";
export const GET_SPACES = "GET_SPACES";
export const SET_SEARCH = "SET_SEARCH";
export const UPDATE_ROOM = "UPDATE_ROOM";
export const SET_FILTERS = "SET_FILTERS";
export const GET_USER = "GET_USER";
export const LOGOUT = "LOGOUT";
