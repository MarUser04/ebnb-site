import React from "react";

//Components
import Gallery from "../components/Detail/Gallery";
import Benefits from "../components/Detail/Benefits";
import Services from "../components/Detail/Services";

//Asset
import StarIcon from "../assets/svg/star.svg";
import Calendar from "../components/Detail/Calendar";
import Form from "../components/Detail/Form";

const DetailPage = ({ room }) => {
  return (
    <div className="flex flex-col mt-14 mx-5 md:mx-18 lg:mx-24">
      <h1 className="mb-1 font-poppins-semibold text-32xl leading-8">
        {room.title}
      </h1>
      <div className="flex flex-col md:flex-row items-start md:items-center mb-6">
        <div className="flex flex-row mr-2">
          <img className="mr-1" src={StarIcon} alt="Star icon" />
          <span className="mr-1 font-inter-semibold text-base leading-">
            {room.stars}
          </span>
        </div>
        <span className="text-gray-400 font-inter text-base leading-5">
          ({room.reviews}) • {room.description}
        </span>
      </div>
      <Gallery />
      <div className="flex flex-col-reverse md:flex-row w-full mt-8 md:mt-16 mb-12">
        <div className="flex flex-col w-full md:w-6/12 lg:w-3/5 pr-0 lg:pr-10">
          <div>
            <h1 className="font-poppins-semibold text-32xl leading-8 mb-1">
              Descubre salas cerca de ti
            </h1>
            <div className="flex flex-col w-full">
              <span className="text-gray-600 text-sm font-inter leading-5">
                {room.description}
              </span>
              <span className="mt-1 text-gray-600 text-sm font-inter-semibold leading-5">
                Flexibilidad de cancelación: {room.flexibility}
              </span>
              <span className="mt-1 text-gray-600 text-sm font-inter-semibold leading-5">
                Tipo de actividad: {room.activity}
              </span>
            </div>
            <div className="mt-8 border-gray-300 border-b w-full"></div>
            <Benefits />
            <div className="my-9 border-gray-300 border-b w-full"></div>
            <Services />
            <div className="my-9 border-gray-300 border-b w-full"></div>
          </div>
          <div className="flex flex-col w-full ">
            <Calendar />
          </div>
        </div>
        <div className="flex flex-col w-full md:w-6/12 lg:w-2/5">
          <Form stars={room.stars} hour={room.arrival} />
        </div>
      </div>
    </div>
  );
};

export default DetailPage;
