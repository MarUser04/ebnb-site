import React from "react";
import { Link } from "react-router-dom";

//Components
import Form from "../components/Home/Form";

//Assets
import HomeImg from "../assets/images/hero - section.png";
import Room from "../components/Home/Room";
import Space from "../components/Home/Space";

import Sala1 from "../assets/images/sala-1.png";
import Sala2 from "../assets/images/sala-2.png";
import Sala3 from "../assets/images/sala-3.png";
import Sala4 from "../assets/images/sala-4.png";
import Sala5 from "../assets/images/sala-5.png";
import Sala6 from "../assets/images/sala-6.png";
import Sala7 from "../assets/images/sala-7.png";
import Sala8 from "../assets/images/sala-8.png";

import Space1 from "../assets/images/space-1.png";
import Space2 from "../assets/images/space-2.png";
import Space3 from "../assets/images/space-3.png";

const IMAGES_ROOMS = [Sala1, Sala2, Sala3, Sala4, Sala5, Sala6, Sala7, Sala8];

const IMAGES_SPACES = [Space1, Space2, Space3];

const HomePage = ({ rooms, spaces, getRoomsByFormFilters, setSearchValue }) => {
  return (
    <>
      <div className="xl:relative flex flex-col-reverse mt-2 xl:mt-0 xl:h-143">
        <img
          className="object-cover w-full h-full"
          src={HomeImg}
          alt="Home forms"
        />
        <div className="xl:absolute xl:ml-36 xl:mt-20  top-1">
          <Form
            getRoomsByFormFilters={getRoomsByFormFilters}
            setSearchValue={setSearchValue}
          />
        </div>
      </div>
      <div className="m-4 my-14 md:m-14">
        <h1 className="text-4xl font-poppins-semibold text-gray-900 text-32xl leading-8">
          Descubre salas cerca de ti
        </h1>
        <div className="grid grid-cols-4 gap-x-64 gap-y-8 mt-9 overflow-auto overflow-x-scroll hide-scroll-bar">
          {rooms.map((room, index) => (
            <Room room={room} key={index} img={IMAGES_ROOMS[index]} />
          ))}
        </div>
      </div>
      <div className="m-4 md:m-10 lg:m-14 h-125 xl:h-135 bg-cover bg-center bg-home-patten bg-no-repeat rounded-2xl">
        <div className="flex flex-col pt-12 pl-4 md:pt-20 md:pl-20">
          <h1 className="text-white text-5xl font-poppins-semibold text-42xl leading-extra">
            Conviértete en <br /> todo un host
          </h1>
          <span className="text-white tracking-wide font-inter font-normal text-base leading-5 pt-1.5">
            Crea y Reserva las reuniones con tus compañeros <br /> para trabajar
            de una manera más comoda.
          </span>
          <Link to="/results">
            <button className="w-max py-3 px-6 mt-6 font-bold rounded-xl font-inter text-base leading-5 bg-white text-gray-900 focus:outline-none">
              Reserva una sala
            </button>
          </Link>
        </div>
      </div>
      <div className="flex flex-col m-5 mt-14 md:mt-0 md:m-11">
        <h1 className="text-4xl ml-0 md:ml-2 font-bold font-poppins-semibold leading-8 text-32xl">
          Descubre experiencias
        </h1>
        <span className="text-xl  ml-0 md:ml-2 tracking-wide font-poppins leading-8">
          Un espacio de trabajo no solo para el trabajo.
        </span>
        <div className="flex flex-col md:flex-row sm:items-center justify-between mt-8">
          {spaces.map((space, index) => (
            <Space space={space} key={index} img={IMAGES_SPACES[index]} />
          ))}
        </div>
      </div>
    </>
  );
};

export default HomePage;
