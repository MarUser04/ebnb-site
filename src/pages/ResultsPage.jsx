import React, { useState } from "react";
import classNames from "classnames";
//Components
import Filters from "../components/Results/Filters";
import ResultInfo from "../components/Results/ResultInfo";
//Assets
import ResultImg1 from "../assets/images/results-1.png";
import ResultImg2 from "../assets/images/results-2.png";
import ResultImg3 from "../assets/images/results-3.png";
import ResultImg4 from "../assets/images/results-4.png";
import ResultImg5 from "../assets/images/results-5.png";

const IMAGES = {
  "result-1": ResultImg1,
  "result-2": ResultImg2,
  "result-3": ResultImg3,
  "result-4": ResultImg4,
  "result-5": ResultImg5,
};

/*
  buttonFilter: filter button active value,
  setButtonFilter: update filter button active method,
  filterRoom: context method for filters rooms with category and subcategory,
  getRooms: get all rooms method from databases,
  filters: filters object value from context. It has main and sub filters,
  setFilters: update filters object in context ,
  setSearchValue: text for search's value
*/

const ResultsPage = ({
  results,
  searchValue,
  updateLikeRoom,
  filterRoom,
  getRooms,
  filters,
  setFilters,
  resetAllRooms,
}) => {
  const [showFilters, setShowFilters] = useState(false);

  return (
    <div className="flex flex-col m-4 md:m-8 lg:mx-24 lg:mt-14">
      <h3 className="text-sm font-inter leading-5">
        {results.length} resultados
      </h3>
      <h1 className="text-black font-poppins-semibold text-32xl leading-8">
        {searchValue
          ? `Resultados de búsqueda para: ${searchValue}`
          : "Mejores Salas"}
      </h1>
      <div className="block md:hidden mt-6">
        <button
          className={classNames(
            "btn-filter-option py-1 mr-2 font-poppins text-sm leading-5",
            {
              "btn-filter-options-selected": showFilters,
            }
          )}
          onClick={() => setShowFilters((prevShowFilters) => !prevShowFilters)}
        >
          Filtros
        </button>
      </div>
      <div
        className={classNames("md:inline", {
          hidden: !showFilters,
        })}
      >
        {searchValue.length === 0 && (
          <Filters
            filterRoom={filterRoom}
            getRooms={getRooms}
            filters={filters}
            setFilters={setFilters}
          />
        )}
      </div>
      <div className="mt-2 md:mt-0">
        <span className="mr-1 text-sm	leading-5 font-inter">
          Antes de reservar, revisa las restricciones relacionadas con el
          coronavirus.
        </span>
        <span className="text-sm font-bold leading-5 font-inter border-black border-b cursor-pointer">
          Más información
        </span>
      </div>
      <button
        onClick={() => resetAllRooms()}
        className="flex flex-row justify-start mt-2 md:mt-1 w-max px-4 py-1 border border-gray-900 rounded-xl leading-5 font-inter-semibold text-gray-900 text-sm focus:outline-none"
      >
        Ver todos los resultados
      </button>
      <div className="mt-2 md:w-full lg:w-3/5 2xl:w-2/5 border-gray-300 border-b"></div>
      <div className="mt-6">
        {results.map((result, index) => (
          <ResultInfo
            result={result}
            key={index}
            img={IMAGES[result.img]}
            lastItem={results.length - 1 === index}
            updateLikeRoom={updateLikeRoom}
          />
        ))}
      </div>
    </div>
  );
};

export default ResultsPage;
