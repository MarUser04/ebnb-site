module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: (theme) => ({
        "home-patten": "url('/src/assets/images/IMG-host.png')",
      }),
      margin: {
        18: "4.4rem",
      },
      width: {
        100: "38rem",
        "1/35": "35%",
      },
      height: {
        125: "25.75rem",
        128: "26.5rem",
        135: "35.75rem",
        143: "43.75rem",
      },
      fontSize: {
        "32xl": "2rem",
        "42xl": "2.875rem",
      },
      lineHeight: {
        extra: "3.125rem",
      },
      transformOrigin: {
        0: "0%",
      },
      zIndex: {
        "-1": "-1",
      },
    },
  },
  variants: {
    borderColor: ["responsive", "hover", "focus", "focus-within"],
    extend: {},
  },
  plugins: [],
};
